# Blender 4.0: Node Editor

## Node Panels

- **Node panels** feature for node groups and built-in nodes
  (blender/blender#108895)
  - Panels can be used to group sockets in a collapsible section. They
    can be added to node groups and to built-in nodes.
- Group sockets are managed in new **UI tree view** together with
  panels. Drag-and-drop support for ordering sockets and inserting into
  panels.
- Node group API has breaking changes, see [Python
  API](python_api.md) for details
  and migration help.

![](../../images/Node_panels_principled_bsdf.png)

## Editing

- The "Make Group" and "Ungroup" operators are no longer available from
  the add menu.
  (blender/blender@7c2dc5183d03d867c100b29059fca83ba9dd8451)
- Node group assets unassigned to any node group are displayed in a
  separate "No Catalog" add menu submenu.
  (blender/blender@d2d4de8c710c4e8e648a86f904e6436133664836)

## Other

- In Preferences → Editing, a new panel was added to group all Node
  Editor specific settings
  (blender/blender@eb57163f).
- Improvements to Node links to help readability at different zooms and
  monitor DPI
  (blender/blender@899d723da8).
- Node snapping grid no longer changes size with changes to line width
  (blender/blender@04285c2d0e).
