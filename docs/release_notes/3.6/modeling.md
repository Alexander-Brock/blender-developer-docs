# Modeling

## UV Editing

### UV Packing

![New **Concave Packing** shape
method](../../images/UVPack36.png "New Concave Packing shape method")
The UV Packing engine was upgraded, dramatically improving performance
on large meshes and improving support for non-square materials.

On many real world cases, efficiency of the layout has also been
increased.

In addition, many new features were implemented:

- Added an option to choose the shape of the UV Packing approximation
  ("Bounding Box", "Exact Shape" and "Convex Hull"), giving layouts
  which use much more of the available space
  (blender/blender@e0d05da8262d84d21a0a1bfb0c7a48d9c29a216c).

<!-- -->

- Added new UV Packing option "Merge Overlapped", where overlapping
  islands stick together during UV packing
  (blender/blender@b601ae87d064cbde29e8ae7191ebba54213c709a).

<!-- -->

- Added new UV Packing option "Pack To \> Original Bounding Box", where
  islands are packed back into the original bounding box of the
  selection.
  (blender/blender@957ac41237fb19e03956dda0a293455b604a95ac).

  

### Improvements

Further UV changes include:

![Unwrapping like an orange-peel. Careful seam placement allows an extended unwrap using the new manual seam placement feature in UV Sphere Projection.](../../images/UVOrange.png)

- **UV Sphere Projection** and **UV Cylinder Projection** now support
  manual placement of seams
  (blender/blender@6b8cdd5979964f39b01bc4def194255fa3766a40).

<!-- -->

- The **UV Select Similar** operator has new options for **Similar
  Winding** and for **Similar Object**.
  (blender/blender@2b4bafeac68e).

  

## Performance

- Conversion from edit meshes to object mode meshes has been
  parallelized, leading to better performance when exiting edit mode
  (blender/blender@5669c5a61b2f049ec1b55cf045221b24095b9df2).
  - The conversion's performance was also improved by around 75% when
    there are multiple UV maps
    (blender/blender@0fe0db63d7f).
- Face corner "split" normal calculation performance is improved by up
  to 80%, with improvements in memory usage as well
  (blender/blender@9fcfba4aae7,
  blender/blender@3c436326517,
  blender/blender@9292e094e77).
  - With custom split normals data, performance has been improved by up
    to 44% in addition
    (blender/blender@17d161f56537d424d369a1dcf4c47d3976842af8).
- Subdivision surface performance is slightly improved on large meshes
  with no loose vertices
  (blender/blender@3507431c306).
- Extracting UV map data for viewport drawing can be up to 3x faster
  (blender/blender@63a44e29aca).

## Compatibility

- When baking normals with custom normals data, behavior may be
  different if the auto smooth angle is not 180 degrees
  (blender/blender#107930).
