# Blender 4.3: Rendering

## Texturing

- A new *Gabor Noise* texture node was added. (blender/blender@4f51033708)