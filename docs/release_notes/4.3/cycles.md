# Blender 4.3: Cycles

## Other

* The Oren-Nayar BSDF (used for Diffuse BSDF with non-zero roughness) now is energy-preserving and accounts for multiscattering. (blender/blender!123345)
  * This results in slightly brighter and more saturated results for existing materials.
