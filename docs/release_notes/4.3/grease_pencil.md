# Blender 4.3: Grease Pencil

Grease Pencil was rewritten to remove deeper limitations and improve overall performance.

## New Features
While the focus of the rewrite was compatibility and feature parity with Grease Pencil v2, there are some new features that come with this release.

### Layer Groups
*TODO*

### Draw Tool Changes
The behavior of the draw tool has been updated.

#### Radius Unit
*TODO*

#### Active Smoothing
*TODO*

#### Screen Space Simplify
*TODO*

### Eraser Tool Changes
*TODO*

### Geometry Nodes
*TODO*

## Performance Improvements
*TODO*

## Miscellaneous Changes
*TODO*

## Conversion & Compatibility
From 4.3 and onwards Grease Pencil objects are automatically converted to the new architecture on file load. There is no backwards compatibility, e.g. Grease Pencil files created or saved in 4.3 or higher will not load correctly in 4.2 or lower.

## Deprecated Features
Some features present in Grease Pencil v2 have been removed. This is mostly due to the changes in the architecture and the difficulty in maintainability. 

### Screen Space Stroke Thickness
Strokes are now always in "World Space" thickness mode.

Note that it is possible to recreate the "Screen Space" thickness effect using a geometry nodes modifier.

### Interpolation Tool Selection Order
The selection order (in edit mode), that was used to specify pairs of strokes to interpolate, has been removed. Porting this feature was not trivial, so it was decided to replace it with a better version in the future.

## Python API Changes
*TODO*