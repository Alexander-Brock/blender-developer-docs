# Blender 4.3: User Interface

## General

- Hover tooltips for the Data-block ID selector shows previews for images, movies, and fonts. (blender/blender@8937c0bcab)
- Tooltips are never resized with 2D region zooming. (blender/blender@1b6910c61c)