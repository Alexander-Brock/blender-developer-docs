# Blender 2.93: EEVEE

## Depth of field

EEVEE's implementation of depth of field has been rewritten from scratch
to improve performance and quality. It also features a new high quality
mode that matches the raytraced ground truth if using a high enough
sample count.
(blender/blender@000a340afa67)

![](../../images/Depth-of-field.jpg)

|Previous implementation|New depth of field|
|-|-|
|![](../../images/Eevee-dof-kitchen-old.jpeg){style="width:500px;"}|![](../../images/Eevee-dof-kitchen-new.jpeg){style="width:500px;"}|

## Volumetrics

A few improvements have been made to the volumetric lighting. It is now
easier to work with and has less limitations.

|Previous implementation|New (bug-fixed) volumetric shadowing|
|-|-|
|![](../../images/1_EEVEE_293_previous_implementation.png){style="width:500px;"}|![](../../images/2_EEVEE_293_fixed_shadowing.png){style="width:500px;"}|

|Area Light Support|Soft Shadows Support|
|-|-|
|![](../../images/3_EEVEE_293_area_light.png){style="width:500px;"}|![](../../images/4_EEVEE_293_soft_shadows.png){style="width:500px;"}|

- Added a new Volume and Diffuse slider light contribution. This
  addition was motivated by the current lack of support of light
  node-tree and visibility flag.
  (blender/blender@884f934a853f)
- Area lights are now correctly supported inside volumetric lighting.
  The solution is not physically based and does not match cycles
  perfectly.
  (blender/blender@355f884b2f09)
- Volumetric Lights Shadowing had bugs fixed that may change the look of
  certain scenes.
  (blender/blender@3a29c19b2bff
  blender/blender@54f52cac7ccd)
- Volume lighting now uses a softer attenuation function, reducing
  flickering of lights center.
  (blender/blender@884f934a853f)
- Volume light clamp was changed to fit the new volumetric lighting.
  (blender/blender@b96acd0663b5)
- Volume shadowing will now be soft if using the soft shadow option in
  the render property panel. Light needs to have shadows enabled for
  this to work.
  (blender/blender@89ef0da5513a)

## Ambient Occlusion

A complete rewrite was done to fix over-darkening artifacts and
precision issues. A better specular occlusion has been implemented to
reduce light leaking from light-probes.
(blender/blender@64d96f68d6ef)

Thanks to this, the ambient occlusion node now supports variable
distance and an approximation of the inverted option.
(blender/blender@dee94afd039d)

![Inverted AO node with textured distance input](../../images/EEVEE_293_ao.png){style="width:400px;"}

## Improvements

- Glass BSDF have been updated and have less visible interpolation
  artifacts when IOR is less than 1.
  (blender/blender@83ac8628c490)
- Fresnel effect of glossy surfaces now follows Cycles more closely.
  (blender/blender@06492fd61984)
- Reflection Cubemaps probes have been improved for low roughness. Old
  scene now require a rebake.
  (blender/blender@aaf1650b0993)
- Normal Maps and smoothed normals do not produce strange reflections
  when reflection is below the surface.
  (blender/blender@1c22b551d0c1)
- Sub-Surface Scattering do not leak light onto nearby surfaces.
  (blender/blender@cd9a6a0f9389)
- Screen-Space Raytracing (Reflections, Refraction, Shadows) have been
  improved to reduce the amount of noise and convergence time.
  (blender/blender@267a9e14f5a7
  blender/blender@bbc5e2605199
  blender/blender@b79f20904170)
- Contact shadow distance fading was removed for performance and
  reliability reasons.
  (blender/blender@6842c549bb3f)
- Faster animation rendering by reusing scene data between frames.
  (blender/blender@50782df42)
