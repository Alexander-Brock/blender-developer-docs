# Geometry Nodes

The geometry nodes project was expanded to build on the attribute
system, allow sampling textures, support volume data, improve usablity,
and much more.

![](../../images/Flower_scattering.jpg)
<a href="https://download.blender.org/demo/geometry-nodes/flower_scattering.blend">Flower scattering sample file</a>
by <a href="https://cloud.blender.org/">Blender Studio</a>.

## New Nodes

![All of the new nodes added in 2.93](../../images/New_Geometry_Nodes_2.93.png){style="width:700px;"}

- Attribute Proximity
  (blender/blender@4d39a0f8eb1103,
  blender/blender@f7933d0744df44)
- Attribute Sample Texture
  (blender/blender@425e706921a8f9)
- Attribute Remove
  (blender/blender@60f7275f7f3cbf)
- Attribute Convert
  (blender/blender@670453d1ecdbb4)
- Attribute Clamp
  blender/blender@43455f385709ed
- Attribute Map Range
  (blender/blender@dda02a448a38)
- Bounding Box
  blender/blender@e0a1a2f49dab57
- Collection Info
  (blender/blender@894cc9c9159204)
- Is Viewport
  (blender/blender@e7af04db0793b6)
- Points to Volume
  (blender/blender@ff7a557c67096f)
- Volume to Mesh
  (blender/blender@16abe9343a5e01)
- Attribute Separate XYZ, Combine XYZ
  (blender/blender@a2ba37e5b6310b)
- Subdivide (simple)
  (blender/blender@4891d4b3d1f38b)
- String Input
  (blender/blender@a961a2189cb38f)
- Mesh Primitive Nodes
  (blender/blender@9a56a3865c06b472a)
  - Cone
  - Cylinder
  - Circle
  - Cube
  - UV Sphere
  - Ico Sphere
  - Line
  - Grid

## New Attributes Available from Mesh Data

- UV layers
  (blender/blender@33a558bf21579b)
- Vertex Colors
  (blender/blender@e3f0b6d5cbe5d3)
- Material Index
  (blender/blender@53bf04f2844b64)
- Face Normals
  (blender/blender@ba3a0dc9ba91)
- Shade Smooth
  (blender/blender@0700441578c9bb)
- Edge Crease
  (blender/blender@3618948df85f)

## Spreadsheet Editor

![The new spreadsheet editor helps to visualize attributes to make it clearer what the nodes are doing](../../images/Spreadsheet_Editor_2.93.png){style="width:600px;"}

- A spreadsheet editor was added to display the values of attributes for
  mesh, point cloud, and instance data
  (blender/blender@3dab6f8b7b89,
  blender/blender@17a5db7303df).
- The value from a geometry at a certain point in the node tree can be
  displayed by clicking on the icon in the node header
  (blender/blender@c6ff722a1fcc).
- Data from a specific node can be pinned to a spreadsheet editor, to
  allow displaying data from more than one node at a time
  (blender/blender@3810bcc16047).

## Usability Improvements

- There is now a default workspace for geometry nodes
  (blender/blender@ac90c8a7743f6d).
- Error messages are now displayed for some problems, like when an input
  attribute doesn't yet exist
  (blender/blender@461d4fc1aae200).
- Clicking on an attribute field now exposes a list of available
  attributes for that node, cached from the latest evaluation
  (blender/blender@461d4fc1aae200).
  - The domain and data type of each attribute are also displayed in the
    search list
    (blender/blender@71eaf872c2db37).

![](../../images/Attribute_Search_2.93.png){style="width:350px;"}

## Rendering Attributes

- Rendering attributes created in geometry nodes is possible with Cycles
  (blender/blender@3a6d6299d7a0f2)
