# Grease Pencil

## Operators

- Sculpt Auto-masking moved to Global setting.
  (blender/blender@4c182aef7ce0)

![](../../images/Auto-masking.png){style="width:600px;"}

- Added new Auto-masking pie menu using `Shift+Alt+A`.
  (blender/blender@4c182aef7ce0)

![](../../images/Auto-masking_radial.png){style="width:600px;"}

- Interpolate Sequence by default can now use all different keyframes
  types as extremes and a new option `Exclude Breakdowns` was added to
  allows users exclude this type of keyframes from the interpolation as
  with the old behavior.
  (blender/blender@56ae4089eb35)

![](../../images/Interpolation.png){style="width:600px;"}

- Copy & Paste now works in multiframe mode.
  (blender/blender@a5d4c63e867e)

## UI

- `Vertex Opacity` parameter now is visible in Overlay panel in Sculpt
  mode.
  (blender/blender@1aff91b1a707)

![](../../images/Vertex_Opacity_Sculpt_Mode.png){style="width:600px;"}

- New option to show the brush size in Draw tool cursor.
  (blender/blender@a44c1284823a)
  (blender/blender@a2cf9a8647fd)

![](../../images/Cursor.png){style="width:600px;"}

- Radial control now displays the correct size when resizing brush size.
  (blender/blender@0fb12a9c2ebc)
- Create new layer using `Y` menu in Drawing mode allows to enter new
  layer name.
  (blender/blender@f53bb93af937)

![](../../images/New_Layer.png){style="width:600px;"}

- Material popover now display Fill color for Fill materials.
  (blender/blender@e144af1f7cd3)

## Modifiers

- Add offset (Location, Rotation, Scale) by Layer, Stroke and Material
  to the `Offset Modifier`.
  (blender/blender@7d712dcd0bbd)

![](../../images/GP_Offset_Modifier.png){style="width:600px;"}

- New `Natural Drawing Speed` mode in `Build Modifier` timing
  property that replay drawings using the recorded speed of the stylus,
  giving it a more natural feel
  (blender/blender@250eda36b8f9)

![](../../images/GP_Build_Modifier.png){style="width:600px;"}
