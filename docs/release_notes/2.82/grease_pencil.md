# Blender 2.82: Grease Pencil

## User Interface

- Added missing Fill tool properties to property editor panel
  (blender/blender@7d3ea8f12ba8)
- Updated Dope Sheet channels/layers
  (blender/blender@98ff6cfa575bbe9680e5a0abf176a9d748ecc2b8)
  - Added the Add New Layer, Remove Layer and Move Up/Down buttons to
    the header
  - Added Opacity, Blend and Onion skinning controls to the Channels
  - Added extra layers properties in the Side panel

![New Dope Sheet channels controls and Side Panel](../../images/Dope_Sheet.png){style="width:800px;"}

- Simplify panel options update
  (blender/blender@96a1bc2997c871a3405dd0a41d780eee49c2c8bb)

## Operators

## Tools

- New eyedropper tool for creating new materials.
  (blender/blender@5adb3b6882f4)
- New Polyline primitive.
  (blender/blender@c2a2cd13be2f)

## Modifiers

- New Multiple Strokes modifier to generate multiple strokes around the
  original ones.
  (blender/blender@91248876e517)

![Multiple Strokes Modifier](../../images/Multiple_Stroke_Modifier.png){style="width:800px;"}
