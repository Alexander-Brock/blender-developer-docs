# Pipeline, Assets, IO

## Asset System & Asset Browser

- Collection assets are now supported
  (blender/blender@253e4e7ed22b)
  - An automatic preview image is generated when using *Mark as Asset*
    (blender/blender@810e225c260d).
  - Collection instancing can be toggled in the *Adjust Last Operation*
    panel after dropping to the 3D View
    (blender/blender@eb1ede569316).
- Ability to drag materials (from Asset Browser for example) to Material
  slots in Properties editor.
  (blender/blender@fd2519e0b694)

## Wavefront Obj I/O

New **experimental OBJ importer**.
(blender/blender@e6a9b223844,
blender/blender@213cd39b6db3)

The new importer is written in C++ and is much faster than the Python
importer, while using less memory as well. Import time comparisons on
several large scenes:

- rungholt.obj Minecraft level (269MB file, 1 object): 54.2s -\> 5.9s,
  memory usage 7.0GB -\> 1.9GB during import.
- Blender 3.0 splash scene (2.4GB file, 24000 objects): 4 hours -\> 53s.

**Experimental OBJ exporter**: Speed improvements.
(blender/blender@1f7013fb90b3,
blender/blender@e2e4c1daaa).

Export time comparisons between Blender 3.1.1 and 3.2:

- Suzanne, subdivided to 6th level (330MB obj file):
  - Windows (32 threads): 6.0s -\> 1.0s
  - macOS (10 threads): 3.9s -\> 1.2s
  - Linux (48 threads): 6.2s -\> 1.4s
- Blender 3.0 splash scene (2.4GB obj file):
  - Windows (32 threads): 45.5s -\> 3.9s
  - macOS (10 threads): 27.2s -\> 5.5s
  - Linux (48 threads): 33.4s -\> 4.4s

## Media Formats

Blender now has support for the WebP image format which works similar to
PNG but compresses faster and generates smaller file sizes.
(blender/blender@4fd0a69d7ba)
