# Layered Animation: Project Baklava

*Project Baklava* is the working title of the in-development layered animation system.
This documentation is tracking the design and development work. At the moment of
writing this intro, this documentation is not complete, but merely a start.

!!! INFO "On the name Baklava"
    The name 'Baklava' is just a project name. It was chosen because the
    project introduces a layered animation system, baklava is layered (and
    delicious), and it nicely alphabetically follows its predecessor
    [Animato](../animato/index.md).

    Eventually this will just be known as *Blender's animation system*.

Relevant links:

- [Project Baklava phase 1: multi-data-block animation][phase1] (projects.blender.org)
- [2024: Animation 2025: Progress & Planning][blogpost-baklava] (code.blender.org)
- [2023: Layered Animation workshop][workshop-2023] (code.blender.org)
- [2022: Character Animation workshop][workshop-2022] (code.blender.org)
- [Layered Animation: Open Questions](https://hackmd.io/@anim-rigging/SkNZ9pLKp) (on HackMD)

[phase1]: https://projects.blender.org/blender/blender/issues/120406
[blogpost-baklava]: https://code.blender.org/2024/02/animation-2025-progress-planning/
[workshop-2023]: https://code.blender.org/2023/07/animation-workshop-june-2023/
[workshop-2022]: https://code.blender.org/2022/11/the-future-of-character-animation-rigging/

## Main Goals

In short, Baklava brings new features to the `Action` data-block:

- Multiple data-blocks can be animated by a single `Action` (and still have different animation).
- Animation can be stored in layers on an `Action` (rather than having separate NLA data-structures per animated data-block).

## Design

For the broad design, see the [2023 workshop][workshop-2023] blog post. Since
then some things were changed / detailed, which are described here.

### Phase 1: Multi-data-block animation

Phase 1 of project Baklava will focus on multi-data-block animation. This makes it possible to have
both an object and its data animated by the same `Action`. Examples are camera transform (object)
and lens parameters (object data).

The initial version of Baklava will not have layers or strips, at least not from the user/UI
perspective. Under the hood animation data is still stored on a layer with a single strip. These are
all implicitly created, though. This makes the whole UI/UX design a lot simpler, and allows us to
focus on the multi-data-block animation part first.

For an up to date plan, see [Project Baklava phase 1: multi-data-block animation][phase1].

### Phase 2: Layered Animation

Phase 2 of project Baklava will bring layered animation to the `Action`. There will likely be other
work done between the two phases, in order for Phase 1 to receive the necessary feedback and
subsequent adjustments.

### Channel Groups & F-Curve Ordering

Legacy `Action`s are separated into 'action groups'. These are arbitrarily named
groupings of F-Curves. This has certain downsides:

- **Grouping:** Some tooling assumes that these are *not* arbitrarily named, and
  makes the assumption that if they match a bone name, all the animation for
  that bone is inside that group. These assumptions can fail, as it's possible
  to arbitrarily rearrange F-Curves and even pop them out of a group, or group
  them manually.
- **F-Curve Order:** F-Curves can be manually reordered within the Action. This
  makes it rather complex for the Blender code to efficiently, say, get all
  rotation components, as this requires a full scan of all the F-Curves.

Proposed design for F-Curves in an `Animation` data-block:

**Grouping** would (for now) be hard-coded and automatically managed by Blender.
These groups would have to exist in DNA in order to store things like
expanded/collapsed state, selection state, and color (for example for bones). It
shall *not* be possible to manually add groups or to move F-Curves between
groups.

Blender would create the following groupings:
  - Property groups: "Transforms", "Custom Properties", maybe some other
    classification of properties.
  - Sub-data groups: Bone name, maybe other sub-data as well.

**F-Curve Order:** F-Curves should be ordered by group, and within that group by
RNA path and array index. This makes it predictable (for both animators and
Blender itself) where they are and how they're ordered. For example, the current
Euler Filter operator assumes that F-Curves for rotation components are
sequentially stored. In practice this is true most of the time, but not always.
Enforcing a fixed ordering will make these operations faster and more
predictable. It is also unclear how much advantage manually reorderable F-Curves
bring to animators.

For groupings of sub-data, the group order should follow the sub-data order. For
bones this would simply be alphabetical, but for shapekeys it should follow the
shapekey order. It is unclear whether this reordering would always happen when
reordering the sub-data, or whether it would be some button to press to sync up
the ordering. Probably the former is more desirable.

**Ordering of Groups** manually should likely still be possible. That way an
animator can, for example, order bones in a sensible-for-the-rig way. A common
example is to order the fingers in actual finger order, instead of
alphabetically.
