# Animation & Rigging

The *Animation & Rigging module* covers the graph editor, dopespheet editor, NLA
editor, keyframes, drivers, constraints, armatures, and more.

Currently the module are working on [Project Baklava][baklava].

[baklava]: ../animation_system/baklava/index.md

## Useful Links

- **[Module Chat][chat]:** the main communication hub of the module. Feel free to pop in and say hi! The title bar of the chat always has links to the latest meeting notes, the video call for the meetings, etc.
- **[Meeting Agenda][agenda]:** calendar of the weekly module meetings.
- **[Tracker][tracker]:** for bug reports, pull requests, list of module members, etc.

[chat]: https://blender.chat/channel/animation-module
[agenda]: https://stuvel.eu/anim-meetings/
[tracker]: https://projects.blender.org/blender/blender/wiki/Module:%20Animation%20&%20Rigging


## Subpages

- [Bigger Projects](bigger_projects.md): broad ideas for the future.
- [Weak Areas](weak_areas.md): areas   of the Animation & Rigging system that could use improvement.
